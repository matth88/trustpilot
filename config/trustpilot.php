<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Auth
    |--------------------------------------------------------------------------
    |
    | ...
    |
    */

    'oauth_token' => env('TRUSTPILOT_OAUTH_TOKEN'),

    /*
    |--------------------------------------------------------------------------
    | Url
    |--------------------------------------------------------------------------
    |
    | ...
    |
    */

    'url'                           => env('TRUSTPILOT_URL'),
    'api_version'                   => env('TRUSTPILOT_API_VERSION'),
    'endpoint_prefix'               => env('TRUSTPILOT_ENDPOINT_PREFIX'),
    'endpoint_create_invitation'    => env('TRUSTPILOT_ENDPOINT_CREATE_INVITATION'),

    /*
    |--------------------------------------------------------------------------
    | Outspot details
    |--------------------------------------------------------------------------
    |
    | ...
    |
    */

    'business_unit_id'  => env('TRUSTPILOT_BUSINESS_UNIT_ID'),
    'sender'            => [
        'name'  => env('TRUSTPILOT_SENDER_NAME'),
        'email' => env('TRUSTPILOT_SENDER_EMAIL'),

    ],
    'reply_to'          => env('TRUSTPILOT_REPLY_TO'),
    'template_id'       => env('TRUSTPILOT_TEMPLATE_ID'),
    'redirect_uri'      => env('TRUSTPILOT_REDIRECT_URI'),

];
