<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpFoundation\Response;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->is('api/*')) {
            $response = $this->renderJson($exception);
        } else {
            $response = parent::render($request, $exception);
        }

        return $response;
    }

    private function renderJson(Exception $exception) : JsonResponse 
    {
        $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;

        if (
            !is_null($exception->getCode()) && 
            $exception->getCode() != 0
        ) {
            $statusCode = $exception->getCode();
        }

        $data = [
            'error' => $exception->getMessage(),
        ];

        return response()->json($data, $statusCode);
    }
}
