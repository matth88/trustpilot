<?php

namespace App\Services;

use App\Models\FeedbackInvitation;
use App\Adapters\TrustpilotAdapter;

class FeedbackInvitationService
{
    /** @var TrustpilotAdapter */
    protected $trustpilotAdapter;

    public function __construct(TrustpilotAdapter $trustpilotAdapter)
    {
        $this->trustpilotAdapter = $trustpilotAdapter;
    }

    public function store(FeedbackInvitation $feedbackInvitation)
    {
        $this->trustpilotAdapter->createNewInvitation($feedbackInvitation);
    }
}