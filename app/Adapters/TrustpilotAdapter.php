<?php

namespace App\Adapters;

use App\Adapters\Adapter;
use App\Models\FeedbackInvitation;
use Config;
use Exception;
use GuzzleHttp\Exception\ClientException;
use Log;

class TrustpilotAdapter extends Adapter
{
    public function createNewInvitation(FeedbackInvitation $feedbackInvitation) 
    {
        $baseUrl = Config::get('trustpilot.url');
        $apiVersion = Config::get('trustpilot.api_version');
        $endpointPrefix = Config::get('trustpilot.endpoint_prefix');

        $businessUnitId = $this->getBusinessUnitId();
        $endpoint = sprintf(
            Config::get('trustpilot.endpoint_create_invitation'),
            $businessUnitId
        );

        $url = $baseUrl . $apiVersion . $endpointPrefix . $endpoint;

        $headers = [
            'Authorization' => $this->getOAuthToken(),
        ];

        $data = [
            'referenceId'       => $feedbackInvitation->getReferenceId(),
            'tags'              => $feedbackInvitation->getTags(),
            'locale'            => $feedbackInvitation->getLocale(),
            'recipientEmail'    => $feedbackInvitation->getRecipientEmail(),
            'senderEmail'       => $feedbackInvitation->getSenderEmail(),
            'templateId'        => $feedbackInvitation->getTemplateId(),
            'replyTo'           => $feedbackInvitation->getReplyTo(),
            'recipientName'     => $feedbackInvitation->getRecipientName(),
            'senderName'        => $feedbackInvitation->getSenderName(),
            'redirectUri'       => $feedbackInvitation->getRedirectUri(),
        ];

        try {
            $this->send('POST', $url, $data);
        } catch (ClientException $ce) {
            Log::error($ce->getMessage());
            throw new Exception('Something went wrong while connecting to Trustpilot\'s API');
        }
    }

    private function getOAuthToken() : string 
    {
        return 'Bearer ' . Config::get('trustpilot.oauth_token');
    }

    private function getBusinessUnitId() : string
    {
        return Config::get('trustpilot.business_unit_id');
    }
}
