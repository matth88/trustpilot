<?php

namespace App\Adapters;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class Adapter
{
    protected function send(string $method, string $url, array $headers = [], array $data = [])
    {
        $client = new Client();

        $client->request(
            $method, 
            $url, 
            [
                'headers'               => $headers,
                RequestOptions::JSON    => $data,
            ]
        );
    }
}
