<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator as ValidatorContract;
use Illuminate\Foundation\Http\FormRequest as LaravelFormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;

abstract class FormRequest extends LaravelFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     * 
     * @return array
     */
    abstract public function rules();

    /**
     * Determine if the user is authorized to make this request.
     * 
     * @return bool
     */
    abstract public function authorize();

    /**
     * Handle a failed validation attempt.
     * 
     * @param ValidatorContract $validator
     * 
     * @throws ValidationException
     */
    protected function failedValidation(ValidatorContract $validator)
    {
        $validationException = new ValidationException($validator);
        $errors = $validationException->errors();

        throw new HttpResponseException(
            response()->json(
                [
                    'errors' => $errors,
                ],
                JsonResponse::HTTP_BAD_REQUEST
            )
        );
    }
}