<?php

namespace App\Http\Controllers;

use App\Factories\FeedbackInvitationFactory;
use App\Http\Requests\StoreFeedbackInvitationRequest;
use App\Services\FeedbackInvitationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response;

class FeedbackInvitationController extends BaseController
{
    /** @var FeedbackInvitationService */
    protected $feedbackInvitationService; 

    public function __construct(FeedbackInvitationService $feedbackInvitationService)
    {
        $this->feedbackInvitationService = $feedbackInvitationService;
    }

    public function store(StoreFeedbackInvitationRequest $request) : JsonResponse
    {
        $feedbackInvitation = FeedbackInvitationFactory::create(
            $request->input('locale'),
            $request->input('recipientEmail'),
            $request->input('recipientName')
        );

        $this->feedbackInvitationService->store($feedbackInvitation);

        return response()->json($feedbackInvitation, Response::HTTP_CREATED);
    }
}
