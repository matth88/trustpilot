<?php

namespace App\Models;

use JsonSerializable;

class FeedbackInvitation implements JsonSerializable
{
    protected $referenceId;
    protected $tags;
    protected $locale;
    protected $recipientEmail;
    protected $senderEmail;
    protected $templateId;
    protected $replyTo;
    protected $recipientName;
    protected $senderName;
    protected $redirectUri;

    public function getReferenceId() : string
    {
        return $this->referenceId;
    }

    public function setReferenceId(string $referenceId)
    {
        $this->referenceId = $referenceId;
    }

    public function getTags() : array
    {
        return $this->tags;
    }

    public function setTags(array $tags)
    {
        $this->tags = $tags;
    }

    public function getLocale() : string
    {
        return $this->locale;
    }

    public function setLocale(string $locale)
    {
        $this->locale = $locale;
    }

    public function getRecipientEmail() : string
    {
        return $this->recipientEmail;
    }

    public function setRecipientEmail(string $recipientEmail)
    {
        $this->recipientEmail = $recipientEmail;
    }

    public function getSenderEmail() : string
    {
        return $this->senderEmail;
    }

    public function setSenderEmail(string $senderEmail)
    {
        $this->senderEmail = $senderEmail;
    }

    public function getTemplateId() : string
    {
        return $this->templateId;
    }

    public function setTemplateId(string $templateId)
    {
        $this->templateId = $templateId;
    }

    public function getReplyTo() : string
    {
        return $this->replyTo;
    }

    public function setReplyTo(string $replyTo)
    {
        $this->replyTo = $replyTo;
    }

    public function getRecipientName() : string
    {
        return $this->recipientName;
    }

    public function setRecipientName(string $recipientName)
    {
        $this->recipientName = $recipientName;
    }

    public function getSenderName() : string
    {
        return $this->senderName;
    }

    public function setSenderName(string $senderName)
    {
        $this->senderName = $senderName;
    }

    public function getRedirectUri() : string
    {
        return $this->redirectUri;
    }

    public function setRedirectUri(string $redirectUri)
    {
        $this->redirectUri = $redirectUri;
    }

    public function jsonSerialize()
    {
        return 
        [
            'referenceId'       => $this->getReferenceId(),
            'tags'              => $this->getTags(),
            'locale'            => $this->getLocale(),

            'recipientName'     => $this->getRecipientName(),
            'recipientEmail'    => $this->getRecipientEmail(),

            'senderName'        => $this->getSenderName(),
            'senderEmail'       => $this->getSenderEmail(),
            'replyTo'           => $this->getReplyTo(),

            'templateId'        => $this->getTemplateId(),
            'redirectUri'       => $this->getRedirectUri(),
        ];
    }
}
