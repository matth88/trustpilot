<?php

namespace App\Factories;

use App\Models\FeedbackInvitation;
use Config;

class FeedbackInvitationFactory
{
    public static function create(
        string $locale, 
        string $recipientEmail, 
        string $recipientName
    ) : FeedbackInvitation {
        $feedbackInvitation = new FeedbackInvitation();

        $feedbackInvitation->setReferenceId('inv00001');
        $feedbackInvitation->setLocale($locale); 
        $feedbackInvitation->setRecipientEmail($recipientEmail); 
        $feedbackInvitation->setRecipientName($recipientName);

        $feedbackInvitation->setTags(
            [
                'tag1',
                'tag2',
            ]
        );

        $feedbackInvitation->setSenderName(
            Config::get('trustpilot.sender.name')
        );

        $feedbackInvitation->setSenderEmail(
            Config::get('trustpilot.sender.email')
        );

        $feedbackInvitation->setReplyTo(
            Config::get('trustpilot.reply_to')
        );

        $feedbackInvitation->setTemplateId(
            Config::get('trustpilot.template_id')
        );

        $feedbackInvitation->setRedirectUri(
            Config::get('trustpilot.redirect_uri')
        );

        return $feedbackInvitation;
    }
}
